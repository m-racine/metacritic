import unittest
from unittest.mock import MagicMock

from metacritic import MetaCritic, ExampleMetaCritic


class ExampleFileScrapeTests(unittest.TestCase):
    def setUp(self):
        self.meta = ExampleMetaCritic()

    def test_scrape_data_from_page(self):
        self.assertEqual("Sunset Overdrive - 81 7.8", str(self.meta))

    def test_check_correct_score(self):
        self.assertEqual(81, self.meta.metacritic)

    def test_check_correct_user_score(self):
        self.assertEqual(7.8, self.meta.userscore)


class LiveSiteScrapeTests(unittest.TestCase):
    def setUp(self):
        self.high = MetaCritic("Mass Effect 2", "PC")
        self.medium = MetaCritic("We Happy Few", "PC")
        self.low = MetaCritic("Bubsy The Woolies Strike Back", "PC")
        self.missing = MetaCritic("Cthulhu Realms", "PC")

    def test_scrape_data_from_page(self):
        self.assertEqual("Mass Effect 2", self.high.game)
        self.assertEqual("We Happy Few", self.medium.game)
        self.assertEqual("Bubsy The Woolies Strike Back", self.low.game)
        self.assertEqual("Cthulhu Realms", self.missing.game)

    # Mass Effect 2, PC is High, High, also a "metacritic must play?"
    # Witcher 2 Assassins of Kings is high high without that
    # River City Girls, Switch is High with med user
    def test_check_correct_high_score(self):
        self.assertEqual(self.high.metacritic, 94)

    def test_check_correct_high_user_score(self):
        self.assertEqual(self.high.userscore, 8.9)
    
    def test_check_correct_medium_score(self):
        self.assertEqual(self.medium.metacritic, 62)
    
    def test_check_correct_medium_user_score(self):
        self.assertEqual(self.medium.userscore, 5.6)
    
    def test_check_correct_low_score(self):
        self.assertEqual(self.low.metacritic, 44)
        
    def test_check_correct_low_user_score(self):
        self.assertEqual(self.low.userscore, 4.8)

    def test_check_correct_empty_score(self):
        self.assertEqual(self.missing.metacritic, -2.0)
    
    def test_check_correct_empty_user_score(self):
        self.assertEqual(self.missing.userscore, -2.0)


# https://www.metacritic.com/search/game/fullmetal%20alchemist/results
# ?page=1 == 2nd page


class ExampleSiteSearchTests(unittest.TestCase):
    def test_search_term_is_accurate(self):
        # set mock object to return ExampleMetacritic when
        # metacritic = MetaCritic()
        # metacritic.
        # metacritic is called for game
        # assert that the valeus returned are the expected ones
        pass

    def test_search_term_is_found_on_page_one(self):
        # set mock object to return the example page one search
        # when the game is queried
        # AND then also return the right example page when the found bit is found
        pass

    def test_search_term_is_found_on_page_two(self):
        # set mock object to return the example page one search
        # when the game is queried
        # and then return the example page 2 when next page is called
        # AND then also return the right example page when the found bit is found
        pass

    def test_search_term_is_not_found_on_any_page(self):
        # set mock object to return the example page one search
        # when the game is queried
        # and then return the example page 2 when next page is called
        # AND then also return the right example page when the found bit is found
        # and then return no results
        pass

    def test_search_term_returns_no_pages(self):
        # set mock object to return no results page
        # return no results
        pass


class LiveSiteSearchTests(unittest.TestCase):
    def test_search_term_is_accurate(self):
        pass

    def test_search_term_is_found_on_page_one(self):
        pass

    def test_search_term_is_found_on_page_two(self):
        pass

    def test_search_term_is_not_found_on_any_page(self):
        pass

    def test_search_term_returns_no_pages(self):
        pass


# can also use timeouts maybe on all of these--or just test that they take under X

# meta = MetaCritic("Cthulhu Realms", "PC")
# print(meta)

# link = meta.raw_data.find("li",attrs={"class":"summary_detail product_genre"})
# print(link)
# links = link.find_all("span",attrs={"class":"data","itemprop":"genre"})
# print(links)
# # -- fails -- print(fixDateFormat(meta.raw_data.find("span",attrs={"class":"data","itemprop":"datePublished"}).text))
# #  -- method missing -- print(normalizeSystem("itch.io"))
# print(fixGame(":?'\;.><#&,'Master of Orion (1993)"))
# print(fixDateFormat("Jun 12, 2001"))

# #print(meta.raw_data)

# # --fails -- link = meta.raw_data.find('li',attrs={"class":"summary_detail publisher"}).find("span",attrs={'itemprop':'name'}).text.strip()
# print(link)

# #might need to use children/descendants to find the inner span
# print(len(list(meta.raw_data.children)))
# # 1
# print(len(list(meta.raw_data.descendants)))
# # 25

# link = meta.raw_data.find("li",attrs={"class":"summary_detail developer"}).find("span",attrs={"class":"data"}).text.strip()
# print(link)
# # -- failes -- link = link.find("span",attrs={"class":"data"})
# print(link)

# print(meta.raw_data.find(user_score_class))
            
