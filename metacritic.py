from bs4 import BeautifulSoup
import requests
import re
import os
import logging
LOGGER = logging.getLogger('MYAPP')
# swapMonth function works better as a dict in python
month_dict = {"Jan": "01", "Feb": "02", "Mar": "03", "Apr": "04",
              "May": "05", "Jun": "06", "Jul": "07", "Aug": "08",
              "Sep": "09", "Oct": "10", "Nov": "11", "Dec": "12"}

# def has_class_but_no_id(tag):
#     return tag.has_attr('class') and not tag.has_attr('id')


def metacritic_class(tag):
    return "class" in tag.attrs and \
           {"metascore_w", "xlarge", "game"}.issubset(tag['class']) and \
           ("user" not in tag["class"])


def user_score_class(tag):
    return "class" in tag.attrs \
           and {"metascore_w", "user", "large", "game"}.issubset(tag['class'])


def fix_system(system):
    # faster to do this first
    system = "-".join(system.lower().split(" "))
    if system in ["steam", "gog", "origin", "humble", "uplay", "twitch", "battle.net",
                  "itch.io", "epic-games", "indiebox", "digipen"]:
        return "pc"
    elif system in ["kindle", "android"]:
        return "ios"
    # system = "-".join(system.lower().split(" "))
    
    # unsure if going to use these base don how systems are stored in the db
    if system == "gba":
        system = "game-boy-advance"
    elif system == "gbc":
        system = "game-boy-color"
    elif system == "n64":
        system = "nintendo-64"
    elif system == "nintendo-3ds":
        system = "3ds"
    elif system == "nintendo-ds":
        system = "ds"
    elif system == "playstation-portable":
        system = "psp"
    elif system == "vita":
        system = "playstation-vita"
    return system


def fix_game(game):
    # to replace \ that's what you need in the re, \\\\
    game = re.sub(r"[:\/\?'\(;\.\)#&$,\\\\']", "", game)
    game = game.replace("<", "lt").replace(">", "gt")
    game = game.replace(" ", "-").lower()
    game = game.replace(u"\xe2\x80\x99", "")
    game = re.sub(r"[\-]{2}", "-", game)
    game = re.sub(r"[\-]{2}", "---", game)
    return game


# def fix_date_format(unformatted_date):
#     # looks like "Jun 12, 2001"
#     # should be "2001-06-12"
#     # Logger.log(date);
#     initial = unformatted_date.split(", ")
#     second = initial[0].replace("  ", " 0").split(" ")
#     return initial[1] + "-" + month_dict[second[0]] + "-" + second[1]


def scrape_metacritic(game, system):
    # fix_game and fix_system are called here to avoid multiple calls later.
    game = fix_game(game)
    system = fix_system(system)
    # fetch metacritic page
    # if page not found, fetch search page
    # look on search pages until no pages left
    # if found, fetch that page.
    # so basically two parse things are needed here -- a landing page and a search page.
    headers = {'User-Agent': 'Mozilla/5.0'}
    url = "http://www.metacritic.com/game/" + system + "/" + game
    request = requests.get(url, headers=headers)
    if request.content['response_code'] == 404:
        return scrape_search(game, system)
    elif request.content['response_code'] == 200:
        return scrape_page(request.text)
    else:
        raise BaseException


def scrape_search(game, system):
    headers = {'User-Agent': 'Mozilla/5.0'}
    url = "http://www.metacritic.com/game/" + system + "/" + game
    #https://www.metacritic.com/search/game/fire%20emblem/results
    request = requests.get(url, headers=headers)
    raw_data = BeautifulSoup(request.text)
    return


def scrape_page(request):
    raw_data = BeautifulSoup(request, "html.parser")
    if re.search("No score yet", raw_data.find("span", attrs={"class", "desc"}).text):
        metacritic = -2.0
    else:
        metacritic = request.find(metacritic_class).text
        if metacritic == "tbd":
            metacritic = -2.0
        else:
            metacritic = int(metacritic)
    # print raw_data.find(user_score_class)
    if raw_data.find(user_score_class):
        if re.search("No score yet", raw_data.find(user_score_class).text):
            userscore = -2.0
        else:
            userscore = raw_data.find(user_score_class).text
            if userscore == "tbd":
                userscore = -2.0
            else:
                userscore = float(userscore)
    else:
        userscore = -2.0
    return metacritic, userscore


class MetaCritic:
    def __init__(self, game, system):
        headers = {'User-Agent': 'Mozilla/5.0'}
        url = "http://www.metacritic.com/game/"+fix_system(system)+"/" + fix_game(game)
        LOGGER.debug(url)
        request = requests.get(url, headers=headers)
        self.raw_data = BeautifulSoup(request.text, "html.parser")
        self.game = game
        # print self.raw_data.find_all("span",attrs={"class":"desc"})
        if self.raw_data.find("title").text == "404 Page Not Found - Metacritic - Metacritic":
            self.metacritic = -1.0
            self.userscore = -1.0
        else:
            if re.search("No score yet", self.raw_data.find("span", attrs={"class", "desc"}).text):
                self.metacritic = -2.0
            else:
                self.metacritic = self.raw_data.find(metacritic_class).text
                if self.metacritic == "tbd":
                    self.metacritic = -2.0
                else:
                    self.metacritic = int(self.metacritic)
            # print self.raw_data.find(user_score_class)
            if self.raw_data.find(user_score_class):
                if re.search("No score yet", self.raw_data.find(user_score_class).text):
                    self.userscore = -2.0
                else:
                    self.userscore = self.raw_data.find(user_score_class).text
                    if self.userscore == "tbd":
                        self.userscore = -2.0
                    else:
                        self.userscore = float(self.userscore)
            else:
                self.userscore = -2.0
            
        # self.publisher = self.raw_data.find('li',attrs={"class":"summary_detail publisher"})
        #   .find("span",attrs={'itemprop':'name'}).text.strip()
        # #can be a list, may need to refactor
        # self.developer = self.raw_data.find("li",attrs={"class":"summary_detail developer"})
        #   .find("span",attrs={"class":"data"}).text.strip()
        # self.release_date = fixDateFormat(self.raw_data
        #   .find("span",attrs={"class":"data","itemprop":"datePublished"}).text)
        # self.players  = self.raw_data.find("li",attrs={"class":"summary_detail product_players"})
        #   .find("span",attrs={"class":"data"}).text.strip()
        # #can be a list
        # self.genre = self.raw_data.find("li",attrs={"class":"summary_detail product_genre"})
        #   .findall("span",attrs={"class":"data","itemprop":"genre"}).text.strip()

    def __str__(self):
        return u"{0} - {1} {2}".format(self.game, self.metacritic, self.userscore)


class ExampleMetaCritic:
    def __init__(self):
        with open(os.path.join(os.cwd(), "example.html"), "r") as f:
            self.raw_data = BeautifulSoup(f, "html.parser")
        self.game = "Sunset Overdrive"
        self.metacritic = int(self.raw_data.find("span", attrs={"itemprop": "ratingValue"}).text)
        self.userscore = float(self.raw_data.find(user_score_class).text)

    def __str__(self):
        return u"{0} - {1} {2}".format(self.game, self.metacritic, self.userscore)


#searching by system will require a mapping

#https://www.metacritic.com/search/game/fire%20emblmem/results?plats[72496]=1&search_type=advanced is an example

# METACRITIC_SYSTEMS = \
#     {'PS4': {"name": "playstation-4", "value": 72496},
#      'PS3': {"name": "playstation-3", "value": 1},
#      "XB1": {"name": "xbox-one", "value": 80000},
#      "XBX": {"name": "xbox", "value": 12},
#      "360": {"name": "xbox-360", "value": 2},
#      "PC": {"name": "pc", "value": 3},
#      "DS": {"name": "ds", "value": 4},
#      "3DS": {"name": "3ds", "value": 16},
#      "VIT": {"name": "playstation-vita", "value": 67365},
#      "PSP": {"name": "psp", "value": 7},
#      "WII": {"name": "wii", "value": 8},
#      "WIU": {"name": "wii-u", "value": 68410},
#      "NSW": {"name": "switch", "value": 268409},
#      "PS2": {"name": "playstation-2", "value": 6},
#      "PSX": {"name": "playstation", "value": 10},
#      "GBA": {"name": "game-boy-advance", "value": 11},
#      "IPO": {"name": "ios", "value": 9},
#      "GCN": {"name": "gamecube", "value": 13},
#      "N64": {"name": "nintendo-64", "value": 14},
#      "DRM": {"name": "dreamcast", "value": 15}}